# Dutch translation of sysvinit debconf templates.
# Copyright (C) 2011, 2012 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the sysvinit package.
# Jeroen Schot <schot@a-eskwadraat.nl>, 2011, 2012.
# Frans Spiesschaert <Frans.Spiesschaert@yucom.be>, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: sysvinit 2.92~beta-2\n"
"Report-Msgid-Bugs-To: sysvinit@packages.debian.org\n"
"POT-Creation-Date: 2018-10-26 08:14+0000\n"
"PO-Revision-Date: 2018-12-03 20:26+0100\n"
"Last-Translator: Frans Spiesschaert <Frans.Spiesschaert@yucom.be>\n"
"Language-Team: Debian Dutch l10n Team <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Gtranslator 2.91.7\n"

#. Type: boolean
#. Description
#: ../sysvinit-core.templates:1001
msgid "Update getty pathnames and add hurd-console?"
msgstr "getty-padnamen bijwerken en hurd-console toevoegen?"

#. Type: boolean
#. Description
#: ../sysvinit-core.templates:1001
msgid ""
"Your /etc/inittab seems to use /libexec/getty as getty and/or to miss hurd-"
"console entry. The default inittab has been changed, however your /etc/"
"inittab has been modified. Note that sysvinit has not been used to boot an "
"Hurd system for long, so any errors in that file would not have shown up "
"earlier."
msgstr ""
"Uw /etc/inittab lijkt /libexec/getty te gebruiken als getty en/of geen hurd-"
"console-item te bevatten. Het standaard inittab is gewijzigd, maar uw /etc/"
"inittab werd lokaal aangepast. Merk op dat sysvinit lange tijd niet gebruikt "
"werd om een Hurd-systeem op te starten, waardoor eventuele fouten in dat "
"bestand niet eerder aan de oppervlakte konden komen."

#. Type: boolean
#. Description
#: ../sysvinit-core.templates:1001
msgid ""
"If you allow this change, a backup will be stored in /etc/inittab.dpkg-old."
msgstr ""
"Indien u akkoord gaat met deze aanpassing, zal een reservekopie bewaard "
"worden in /etc/inittab.dpkg-old."

#. Type: boolean
#. Description
#: ../sysvinit-core.templates:1001
msgid ""
"If you don't allow this change, an updated inittab will be written to /etc/"
"inittab.dpkg-new. Please review the changes and update your /etc/inittab "
"accordingly."
msgstr ""
"Indien u niet akkoord gaat met deze aanpassing, zal een bijgewerkte inittab "
"opgeslagen worden in /etc/inittab.dpkg-new. Gelieve de aanpassingen na te "
"kijken en uw eigen /etc/inittab dienovereenkomstig bij te werken."

#~ msgid "Unable to migrate to dependency-based boot system"
#~ msgstr "Niet mogelijk om te migreren naar de nieuwe opstartvolgorde"

#~ msgid ""
#~ "Problems in the boot system exist which are preventing migration to "
#~ "dependency-based boot sequencing:"
#~ msgstr ""
#~ "Er zijn problemen gevonden in het opstartsysteem die het migreren naar de "
#~ "nieuwe opstartvolgorde verhinderen:"

#~ msgid ""
#~ "If the reported problem is a local modification, it needs to be fixed "
#~ "manually.  These are typically due to obsolete conffiles being left after "
#~ "a package has been removed, but not purged.  It is suggested that these "
#~ "are removed by running:"
#~ msgstr ""
#~ "Als het gerapporteerde probleem een lokale wijziging is dient u dit "
#~ "handmatig op te lossen. Deze worden meestal veroorzaakt door verouderde "
#~ "conffiles die achter zijn gebleven nadat een pakket is verwijderd, maar "
#~ "niet gewist ('purge'). U wordt aangeraden deze te verwijderen door het "
#~ "uitvoeren van:"

#~ msgid "${SUGGESTION}"
#~ msgstr "${SUGGESTION}"

#~ msgid ""
#~ "Package installation can not continue until the above problems have been "
#~ "fixed.  To reattempt the migration process after these problems have been "
#~ "fixed, run \"dpkg --configure sysv-rc\"."
#~ msgstr ""
#~ "De pakketinstallatie kan niet doorgaan totdat de bovenstaande problemen "
#~ "zijn opgelost. Na het oplossen van de problemen kunt u het migratieproces "
#~ "nogmaals proberen door \"dpkg-reconfigure sysv-rc\" uit te voeren."
